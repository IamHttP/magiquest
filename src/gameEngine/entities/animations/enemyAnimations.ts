import charImageURL from 'assets/sentry.png';
import commonAnimations from "entities/animations/characterAnimations";

const enemyAnimations = {
  ...commonAnimations(charImageURL)
};

export default enemyAnimations;