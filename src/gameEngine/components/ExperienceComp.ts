import {EXPERIENCE_COMP} from './ComponentNamesConfig';

class ExperienceComp {
  name:string;
  XP:number;
  constructor() {
    this.name = EXPERIENCE_COMP;
    this.XP = 0;
  }
}

export default ExperienceComp;